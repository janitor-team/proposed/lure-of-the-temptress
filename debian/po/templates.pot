# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2013-02-08 17:42+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: lure:10
#, sh-format
msgid "Usage: lure [OPTION]"
msgstr ""

#: lure:11
#, sh-format
msgid "Start Lure of the Temptress"
msgstr ""

#: lure:13
#, sh-format
msgid "--help        display this help and exit"
msgstr ""

#: lure:14
#, sh-format
msgid "--version     display version information and exit"
msgstr ""

#: lure:15
#, sh-format
msgid "--de          play the German version of Lure"
msgstr ""

#: lure:16
#, sh-format
msgid "--fr          play the French version of Lure"
msgstr ""

#: lure:17
#, sh-format
msgid "--it          play the Italian version of Lure"
msgstr ""

#: lure:18
#, sh-format
msgid "--es          play the Spanish version of Lure"
msgstr ""

#: lure:23
#, sh-format
msgid "Lure of the Temptress 1.1"
msgstr ""

#: lure:24
#, sh-format
msgid "Copyright (C) Revolution Software Ltd."
msgstr ""

#: lure:25
#, sh-format
msgid "ScummVM engine Copyright (C) The ScummVM Team"
msgstr ""
