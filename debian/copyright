Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Lure of the Temptress
Source: http://scummvm.org
Comment:
 The game was repacked. The different language versions of Lure can be
 downloaded separately but were merged into one source package together with
 lure.dat. The windows binary, the *.ega files, notes.txt and duplicate
 Disk3.vga files were removed.

Files: *
Copyright: 1992, Revolution Software Ltd <http://www.revolution.co.uk/>.
License: Lure-of-the-Temptress-License
Comment:
 Lure of the Temptress (Lure) is one of the games which were made freely
 available by Revolution Software Ltd. The other one is Beneath a Steel Sky
 (BASS). Both share many similarities with Flight of the Amazon Queen (FOTAQ).
 The original source code of Lure of the Tempress was lost. The same applies to
 the documentation of the game. Hence the data included in the source package
 represents the preferred form for modifications. All three games share the
 same license thus the following text from README.Debian of the BASS package is
 applicable to Lure, too.
 .
 Over the years there have been several bugs questioning the fact why "Flight
 of the Amazon queen" and "Beneath a Steel Sky" were accepted into the main
 section of the Debian archive by FTP masters.
 .
 First of all, both FOTAQ and BASS are fully DFSG compliant. All
 the rights mandated by the DFSG are satisfied by their BSD-like
 license.
 .
 The contrib section of the archive is meant for code which requires
 out of archive tools to compile program source, e.g. a library
 not packaged or a non-free compiler.
 .
 Both FOTAQ and BASS do not need to be compiled, they are a
 aggregation of media which is interpreted by free software in
 the archive (scummvm).
 If they were licensed under the G P L it would fail the "preferred
 form of modification" requirement, but its BSD-like license
 grants you all the necessary rights to modify, use and distribute them.
 .
 While there likely was, once upon a time, a custom set of tools
 to create this game data, those tools do not exist any more.
 The original creators of the game are in the same situation as
 Debian's users when it comes to modifications.
 .
 Also, the reason for requiring the "preferred form for modification"
 is to not put the creator of the software/data in a "monopoly"
 situation. This isn't the case here.

Files: debian/*
Copyright: 2013-2021, Markus Koschany <apo@debian.org>
           2015,      Alexandre Detiste
License: Lure-of-the-Temptress-License

License: Lure-of-the-Temptress-License
 1) You may distribute this game for free on any medium, provided this license
    and all associated copyright notices and disclaimers are left intact.
 .
 2) You may charge a reasonable copying fee for this archive, and may
    distribute it in aggregate as part of a larger & possibly commercial
    software distribution (such as a Linux distribution or magazine coverdisk).
    You must provide proper attribution and ensure this license and all
    associated copyright notices, and disclaimers are left intact.
 .
 3) You may not charge a fee for the game itself. This includes reselling the
    game as an individual item.
 .
 4) You may modify the game as you wish. You may also distribute modified
    versions under the terms set forth in this license, but with the additional
    requirement that the work is marked with a prominent notice which states
    that it is a modified version.
 .
 5) All game content is (C) Revolution Software Ltd.
 .
 6) THE GAMEDATA IN THIS ARCHIVE IS PROVIDED "AS IS" AND WITHOUT ANY EXPRESS OR
    IMPLIED WARRANTIES, INCLUDING AND NOT LIMITED TO ANY IMPLIED WARRANTIES OF
    MERCHANTIBILITY AND FITNESS FOR A PARTICULAR PURPOSE.
